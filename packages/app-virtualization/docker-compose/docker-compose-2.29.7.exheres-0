# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user=docker project=compose tag="v${PV}" ]

SUMMARY="Multi-container orchestration for Docker"
HOMEPAGE+=" https://www.docker.com/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.21]
    run:
        app-virtualization/moby[>=20.10]
"

# need a running docker instance
RESTRICT="test"

DEFAULT_SRC_COMPILE_PARAMS=(
    VERSION=v${PV}
)

src_unpack() {
    default
    export GOMODCACHE="${FETCHEDDIR}/go/pkg/mod"

    esandbox disable_net

    edo pushd "${WORK}"
    edo go mod download -x
    edo popd

    esandbox enable_net
}

src_install() {
    exeinto /usr/$(exhost --target)/libexec/docker/cli-plugins
    doexe ./bin/build/${PN}

    # provide a Compose V1 alias like Docker Desktop does: https://docs.docker.com/compose/migrate
    dodir /usr/$(exhost --target)/bin
    dosym /usr/$(exhost --target)/libexec/docker/cli-plugins/${PN} /usr/$(exhost --target)/bin/${PN}
}

