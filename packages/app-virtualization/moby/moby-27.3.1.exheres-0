# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2017 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Copyright 2020 Maxime Sorin <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ]
require systemd-service [ systemd_files=[ contrib/init/systemd ] ]
require openrc-service s6-rc-service
require udev-rules [ udev_files=[ contrib/udev ] ]

SUMMARY="ship and run any application as a lightweight container"
DESCRIPTION="
A collaborative project for the container ecosystem to assemble
container-based systems
"
HOMEPAGE+=" https://mobyproject.org/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Some dependencies are from https://github.com/moby/moby/tree/v${PV}/hack/dockerfile/install
# Check each file to know the commit / version of each subproject
DEPENDENCIES="
    build:
        dev-lang/go[>=1.22.7]
        dev-golang/go-md2man
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        sys-fs/btrfs-progs
        sys-libs/libcap
        sys-libs/libseccomp
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        group/docker
        net-firewall/iptables
        net-misc/bridge-utils
        sys-apps/containerd[~1.7.22]
        sys-apps/iproute2
        sys-apps/runc[~1.1.14]
        sys-apps/tini[=0.19.0]
"

# Testsuite requires to rebuild too and needs a running docker daemon
RESTRICT="test"

docker_make() {
    DOCKER_CROSSPLATFORMS="linux/386 linux/arm darwin/amd64 darwin/386 freebsd/amd64 freebsd/386freebsd/arm" \
    AUTO_GOPATH=1 \
    IAMSTATIC='false' \
    DOCKER_GITCOMMIT="v${PV}" \
    VERSION="${PV}" \
    GO111MODULE="auto" \
    edo ./hack/make.sh "$@"
}

src_prepare() {
    default
    edo sed -i "s:/usr/bin/:/usr/$(exhost --target)/bin/:" \
        contrib/init/systemd/docker.service \
        contrib/init/openrc/{docker.confd,docker.initd}
}

src_compile() {
    docker_make dynbinary-daemon dynbinary-proxy
}

src_test() {
    docker_make dyntest
}

src_install() {
    local bindir=/usr/$(exhost --target)/bin

    dobin bundles/dynbinary-daemon/dockerd
    dobin bundles/dynbinary-proxy/docker-proxy
    dosym containerd ${bindir}/docker-containerd
    dosym containerd-shim ${bindir}/docker-containerd-shim
    dosym runc ${bindir}/docker-runc
    dosym ctr ${bindir}/docker-containerd-ctr
    dosym tini-static ${bindir}/docker-init

    install_s6-rc_files
    install_systemd_files
    install_udev_files
    newinitd contrib/init/openrc/docker.initd docker
    newconfd contrib/init/openrc/docker.confd docker
    openrc_expart /etc/init.d/docker
    openrc_expart /etc/conf.d/docker

    newbin contrib/check-config.sh check-docker-config.sh

    emagicdocs
}

pkg_postinst() {
    elog "In order to check your kernel/system configuration works with"
    elog "docker, run:"
    elog "  check-docker-config.sh"
}

